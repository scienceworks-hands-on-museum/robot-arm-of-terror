#include <Servo.h>

#define NUM_AXES 6
#define LED_PIN 13
#define MS_DELAY 15
#define CALIBRATE_MOTOR_PIN 1
#define MIN_HEIGHT_MM -75
#define MIN_X_POS_MM 50
#define AXIS_REST_ANGLE {90, 135, 135, 90, 0, 90}

Servo servos[NUM_AXES];

struct coords {
  double x;
  double y;
};

// Electronics.
int axisPlusPin[NUM_AXES] = {22, 24, 26, 28, 30, 32};
int axisMinusPin[NUM_AXES] = {23, 25, 27, 29, 31, 33};
int axisMotorPin[NUM_AXES] = {2, 3, 4, 5, 6, 7};

// Kinematics.
int axisMinAngle[NUM_AXES] = {0, 0, 0, 0, 0, 60};
int axisAngle[NUM_AXES] = AXIS_REST_ANGLE;
int axisMaxAngle[NUM_AXES] = {180, 135, 180, 180, 180, 125};
// Indices calibrated so far: 1, 2, 3, 5
double axisLengthMM[NUM_AXES] = {0, 105.77, 66.91, 160, 0, 0};
double axisZero[NUM_AXES] = {90, 90, 90, 90, 90, 90}; //{0, 0, 46, 180, 0, 0};
double axisDirection[NUM_AXES] = {1, 1, -1, 1, 1, 1};

/**
 * Returns the angle of a given axis in radians,
 * Given a change to an axis.
 */
double getAxisWorldAngleRadians(int axis, int deltaAxis, int delta) {
  double angle = axisAngle[axis];

  // Note: delta is in degrees, so we add it before conversion to radians.
  if (axis == deltaAxis) angle += delta;

  // Account for axis calibration.
  angle = axisDirection[axis] * (angle - axisZero[axis]);

  return (angle * 71.0) / 4068.0;
}

/**
 * Get the position of the end effector,
 * given a delta in radians to the position of a given axis.
 */
coords getEndEffectorPosition(int deltaAxis, int delta) {
  double theta = 0;
  double x = 0;
  double y = 0;

  for (int i = 1; i < NUM_AXES - 2; i++) {
    theta += getAxisWorldAngleRadians(i, deltaAxis, delta);
    double length = axisLengthMM[i];

    x += length * cos(theta);
    y += length * sin(theta);
  }

  coords result = {x, y};

  return result;
}

/**
 * Determine if movement would cause arm to crash.
 */
bool axisPositionCollides(int deltaAxis, int delta) {
  coords pos = getEndEffectorPosition(0, 0);
  coords posNew = getEndEffectorPosition(deltaAxis, delta);

  Serial.print("X position: ");
  Serial.println(pos.y);

  Serial.print("Y position: ");
  Serial.println(pos.y);

  return posNew.x < MIN_X_POS_MM || posNew.y < MIN_HEIGHT_MM;
}

/**
 * Try to move the arm on a particular axis.
 * Avoid colliding with the ground.
 */
void tryMoveAxis(int i, int delta) {
  digitalWrite(LED_PIN, HIGH);

  // if (axisPositionCollides(i, delta)) {
  //   Serial.print("Position would collide on axis ");
  //   Serial.print(i);
  //   Serial.print('\n');
  // } else {
    axisAngle[i] += delta;

    if (axisAngle[i] > axisMaxAngle[i]) {
      axisAngle[i] = axisMaxAngle[i];
    }
    if (axisAngle[i] < axisMinAngle[i]) {
      axisAngle[i] = axisMinAngle[i];
    }

  // }

   Serial.print("{");
   for (int i = 0; i < NUM_AXES; i++) {
      Serial.print(axisAngle[i]);

      if (i < NUM_AXES - 1) Serial.print(',');
   }
   Serial.println("}");
}

void setup() {
  Serial.begin(9600);

  pinMode(LED_PIN, OUTPUT);

  for (int i = 0; i < NUM_AXES; i++) {
    servos[i].attach(axisMotorPin[i]);
    pinMode(axisPlusPin[i], INPUT);
    pinMode(axisMinusPin[i], INPUT);
  }
}

void loop() {

  for (int i = 0; i < NUM_AXES; i++) {
    if (digitalRead(axisPlusPin[i]) == HIGH) {
      tryMoveAxis(i, 1);
    } else if (digitalRead(axisMinusPin[i]) == HIGH) {
      tryMoveAxis(i, -1);
    } else {
      digitalWrite(LED_PIN, LOW);
    }  

    servos[i].write(axisAngle[i]);
  }

  // If input has started
  // And no input for last N seconds
  // Reset arm.

  delay(MS_DELAY);
}
